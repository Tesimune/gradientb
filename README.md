Introduction
Gradient Background (GradientB) is a web platform with a free tool that lets you create a gradient background for websites. The site is also filled with custom colorful gradients from technical web gradient to gradient examples like Stripe, Instagram, and many more.

![Screenshot 2022-02-27 032337.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1645928833978/dySbT9Bpo.png)

Why make this tool?
A Gradient is a unique color property that shows the interaction between two or multiple colors combination. Also, with a mission to build a better internet, one digital project at a time. From building Cool Backgrounds another free design tool to generate background wallpaper for websites, blogs, and phones.

What makes GradientB different from other gradient sites?
Unlike other gradient sites, applications, or platforms. GradientB is packed with unique properties, one of which is powerful preview features, which supports HTML CSS, Tailwind, and Bootstrap tags and class properties.

![Screenshot 2022-02-27 032612.png](https://cdn.hashnode.com/res/hashnode/image/upload/v1645928862661/qPD7Ucac4.png)


[Visit GradientB](https://gradientb.netlify.app/)
[Github](https://github.com/Tesimune/gradientbg)

GradientB stack
•	HTML CSS JavaScript
•	Tailwind CSS
•	NextJS
